import sys

import unidecode

lang = sys.argv[1] if len(sys.argv) > 1 else "french"

acceptable_worlds = [
    unidecode.unidecode(world.strip()).upper()
    for world in open(f"/usr/share/dict/{lang}")
    if len(world.strip()) == 5 and "'" not in world
]

print(acceptable_worlds)
